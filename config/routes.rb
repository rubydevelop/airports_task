Airports::Application.routes.draw do

  root to: "airports#show" 

  resources :airplane_takeoffs, only: [:create]

end
