# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141103101633) do

  create_table "airplane_takeoffs", :force => true do |t|
    t.integer  "airplane_id"
    t.integer  "landing_strip_id"
    t.string   "takeoff_status",   :default => "PENDING"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  add_index "airplane_takeoffs", ["id", "airplane_id", "landing_strip_id"], :name => "airplane_strip_index", :unique => true

  create_table "airplanes", :force => true do |t|
    t.string   "name"
    t.float    "time_for_take_off"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "airports", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "landing_strips", :force => true do |t|
    t.integer  "airport_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "landing_strips", ["id", "airport_id"], :name => "airport_id", :unique => true

end
