class CreateAirports < ActiveRecord::Migration
  
  def self.up
    create_table :airports do |t|
      t.string :name 
      t.timestamps
    end

    Airport.create(name: "Hartsfield Jakson Atlanta International Airoport")
  end

  def self.down
    drop_table :airports
  end
end
