class CreateAirplanes < ActiveRecord::Migration

  def self.up
    create_table :airplanes do |t|
      t.string :name
      t.float :time_for_take_off
      t.timestamps
    end

    Airplane.create(name: "Boeing 707", time_for_take_off: 11.3)
    Airplane.create(name: "Boeing 727", time_for_take_off: 16.7)
    Airplane.create(name: "Boeing 737", time_for_take_off: 21.9)

  end

  def self.down
    drop_table :airplanes
  end
end
