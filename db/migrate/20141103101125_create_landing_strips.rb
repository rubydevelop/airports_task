class CreateLandingStrips < ActiveRecord::Migration

  def self.up
    create_table :landing_strips do |t|
      t.belongs_to :airport
      t.timestamps
    end

    add_index(:landing_strips, [:id, :airport_id], unique: true, name: "airport_id")

    LandingStrip.create(airport: Airport.first)
  end

  def self.down
    remove_index(:landing_strips, name: 'airport_id')
    drop_table :landing_strips
  end
end
