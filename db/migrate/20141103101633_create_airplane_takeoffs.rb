class CreateAirplaneTakeoffs < ActiveRecord::Migration

  def change
    create_table :airplane_takeoffs do |t|
      t.belongs_to :airplane
      t.belongs_to :landing_strip
      t.string :takeoff_status, default: "PENDING"
      t.timestamps
    end

    add_index(:airplane_takeoffs, [:id, :airplane_id, :landing_strip_id], unique: true, name: 'airplane_strip_index')
  end

end
