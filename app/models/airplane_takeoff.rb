require 'render_anywhere'

class AirplaneTakeoff < ActiveRecord::Base

  include RenderAnywhere
  
  default_scope order("id desc")

  BROADCAST_TAKEOFF_STATUS_CHANNEL = "/change_status_for_takeoff"
  TAKING_OFF_STATUS = "TAKING OFF"
  TOOK_OFF_STATUS = "TOOK OFF"
  ADD_NEW_TAKEOFF_PARTIAL = "airplane_takeoffs/add_new_takeoff"

  attr_accessible :airplane_id, :landing_strip_id

  belongs_to :airplane
  belongs_to :landing_strip

  validates :airplane, presence: true
  validates :landing_strip, presence: true

  after_save :broadcast_takeoff_status

  after_create :add_takeoff_to_query

  private

  def broadcast_takeoff_status
    Thread.new do
      data = render template: ADD_NEW_TAKEOFF_PARTIAL, locals: { airplane_takeoff: self }, layout: false
      message = {channel: BROADCAST_TAKEOFF_STATUS_CHANNEL, data: data , ext: { auth_token: FAYE_TOKEN } }
      uri = URI.parse(FAYE_URL)
      Net::HTTP.post_form(uri, message: message.to_json)
    end
  end

  def add_takeoff_to_query
    Thread.new do
      TAKEOFF_SEMAPHORE.synchronize do
        self.takeoff_status = TAKING_OFF_STATUS
        self.save
        sleep self.airplane.time_for_take_off
        self.takeoff_status = TOOK_OFF_STATUS
        self.save
      end
    end
  end


  
end
