class Airport < ActiveRecord::Base

  attr_accessible :name

  has_one :landing_strip

end
