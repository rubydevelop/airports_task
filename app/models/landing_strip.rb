class LandingStrip < ActiveRecord::Base
  
  attr_accessible :airport
  belongs_to :airport

  has_many :airplane_takeoffs

end
