class AirplaneTakeoffsController < ApplicationController
  def create
    @airplane_takeoff = AirplaneTakeoff.new params[:airplane_takeoff]
    @airplane_takeoff.save

    # => return text "" response will be sent via sockets - faye
    render text: "" and return
  end
end