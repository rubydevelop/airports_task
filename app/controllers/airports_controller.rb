class AirportsController < ApplicationController
  def show
    @airport = Airport.includes(landing_strip: :airplane_takeoffs).first
    @airplanes = Airplane.all
  end
end
