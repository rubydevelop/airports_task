module AirportHelper
  def add_airplane_to_landing_strip(airplane_id, landing_strip_id)
    params = { airplane_id: airplane_id, landing_strip_id: landing_strip_id }
    link_to "Add to landing strip", airplane_takeoffs_path(airplane_takeoff: params), 
                                            remote: true, class: "btn btn-success", method: :post
  end
end
